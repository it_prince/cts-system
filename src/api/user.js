import request from '@/utils/request'

export function login(data) {
  return request({
    url: 'http://122.51.249.55:7005/admin/index/login',
    method: 'post',
    data
  })
}

export function getUserInfo() {
  return request({
    url: 'http://122.51.249.55:7005/admin/index/userinfo',
    method: 'post'
  })
}

export function config() {
  return request({
    url: 'http://122.51.249.55:7005/admin/index/config'
  })
}

export function getUserDetailById() {}
export function logout() {}
