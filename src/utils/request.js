import axios from 'axios'
import store from '@/store'
import router from '@/router'
import { Message } from 'element-ui'

// 创建axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // 读取环境变量下的VUE_APP_BASE_API的值作为基准地址
  timeout: 5000
})

service.interceptors.request.use(config => {
  // TODO:这里不一定是store.getters.token
  if (store.getters.token) {
    // TODO: 不一定是Authorization； 值不一定前面要加Bearer  和后端去沟通的！
    config.headers['Authorization'] = `Bearer ${store.getters.token}`
  }

  return config
}, error => {
  return Promise.reject(error)
})

service.interceptors.response.use(response => {
  // TODO： 根据实际情况调整
  const { success, msg, data } = response.data
  if (success) {
    return data
  } else {
    Message.error(msg)
    return Promise.reject(new Error(msg))
  }
}, error => {
  console.log(error)
  //  TODO： 根据实际情况调整，不一定是 10002 不一定是按照code
  if (error.response && error.response.data && error.response.data.code === 10002) {
    store.dispatch('user/logout') // 登出action 删除token
    router.push('/login')
  } else {
    Message.error(error.msg) // 提示错误信息
  }
  return Promise.reject(error)
})

export default service
