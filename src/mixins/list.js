// 什么是混入： 其实就是vue组件的配置项
// 目的：抽离复用相同的功能

export default {
  data(){
    return{
      list:[],
      desc:''
    }
  },
  created(){
    this.initData()
  },
  methods:{
    changeDesc(str){
      this.desc = str
    },
  },
  // 配置项
}

// 混入原则： 
  //  data里面有变量  混入也有   那么就用 data的为主！
  //  methods有方法，混入也有，   那么就用 data的为主！
  // 生命周期是合并！都执行，先执行混入再执行页面！