import store from "@/store";

export default {
  methods:{
    checkPermission(str){
      // str 就是按钮对应的权限标识
      // 思路：判断该权限标识是否在该用户的按钮权限标识数组中
      // 如果在说明 有该按钮权限； 如果不在说明无该按钮权限
      const userInfo = store.state.user.userInfo
      if(userInfo && userInfo.roles && userInfo.roles.points){
        return userInfo.roles.points.includes(str)
      }
      return false
    },
  }
}