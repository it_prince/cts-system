// 导入静态的路由 和 完整的动态路由
import { constantRoutes } from '@/router'
export default {
  namespaced: true,
  state: {
    // 这个主要是系统的侧边栏渲染菜单的！
    routes: constantRoutes // 用户最终完整的动态路由+静态路由  (最开始只有静态路由)
  },
  mutations: {
    // 修改routes, 传入动态的路由
    setRoutes(state, newRoutes) {
      // 静态路由+动态的路由
      state.routes = [...constantRoutes, ...newRoutes, { path: '*', redirect: '/404', hidden: true }]
    }
  },
  actions: {
    // 过滤出属于当前用户的动态路由
    filterRoutes(context, menus) {
      // TODO: 这个筛选逻辑也是要处理的； 不一定是根据name匹配；也有可能服务器直接返回动态路由映射，在这里处理component字段变为对象
      // 思路1：根据name进行匹配筛选
      // 思路2：将动态路由映射处理一下component字段，直接有了动态路由映射

      // 下面这个代码是思路1
      // const userAsyncRoutes = []
      // menus.forEach(item=>{
      //   let row = asyncRoutes.find(val=>val.name === item)
      //   row && userAsyncRoutes.push(row)
      // })
      // context.commit('setRoutes',userAsyncRoutes)
      // return userAsyncRoutes
    }
  }
}
