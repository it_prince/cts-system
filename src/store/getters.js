const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.userInfo.avatar,
  name: state => state.user.userInfo.username
  // companyName: state => state.user.userInfo.companyName,
  // departmentName: state => state.user.userInfo.departmentName,
  // userId: state => state.user.userInfo
  // staffPhoto: state => state.user.userInfo.staffPhoto,
  // companyId: state => state.user.userInfo.companyId
}
export default getters
