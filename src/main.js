import Vue from 'vue'

// 导入基础样式
import 'normalize.css/normalize.css'
// 导入element-ui
import ElementUI from 'element-ui'
// 导入element-ui  样式
import 'element-ui/lib/theme-chalk/index.css'
//  导入i18n实例
import i18n from '@/lang'
// 导入全局样式
import '@/styles/index.scss'
// 导入根组件
import App from './App'
// 导入仓库
import store from './store'
// 导入路由
import router from './router'
// 导入
import Components from '@/components'
// 导入图标
import '@/icons'
// 导入权限控制
import '@/permission'
// 导入所有自定义指令
import * as directives from '@/directives'
// 导入所有过滤器
import * as filters from '@/filters'
// 导入权限校验函数混入方法
import checkPermission from '@/mixins/checkPermission'
// 全局注册混入  [所有组件都有这个混入对象]
Vue.mixin(checkPermission)
// 安装ElementUI，并且配置18n插件
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})
// 安装全局组件插件
Vue.use(Components)
// 关闭提示
Vue.config.productionTip = false
// 批量注册自定义指令
Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key])
})
// 批量注册过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

if (process.env.NODE_ENV === 'development') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// 实例化vue
new Vue({
  el: '#app',
  router,
  store,
  i18n, // 挂载i18n, 然后所有的组件都会有个$t的方法
  render: h => h(App)
})
