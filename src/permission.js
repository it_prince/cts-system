// 权限拦截 导航守卫 路由守卫  router
import router from '@/router' // 引入路由实例
import store from '@/store' // 引入vuex store实例
import NProgress from 'nprogress' // 引入一份进度条插件
import 'nprogress/nprogress.css' // 引入进度条样式

const whiteList = ['/login', '/404'] // 定义白名单  所有不受权限控制的页面
// 路由的前置守卫
router.beforeEach(async function(to, from, next) {
  NProgress.start()
  if (store.getters.token) {
    if (to.path === '/login') {
      next('/')
    } else {
      // TODO: 不一定是getters里面的userId,根据实际情况修改
      if (!store.getters.userId) {
        await store.dispatch('user/getUserInfo')
        // ///////////////////////////权限筛选逻辑/////////////////////////// TODO
        // const { roles } =  await store.dispatch('user/getUserInfo')
        // let userAsyncRoutes = await store.dispatch('permission/filterRoutes',roles.menus)
        // router.addRoutes(userAsyncRoutes)  // 【可以理解异步】
        // next(to.path)  // next(地址)  会让导航重新再来一次，再走一次导航守卫
        // ////////////////////////////////////////////////////// TODO
      }
      next() // 直接放行
    }
  } else {
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login') // 跳到登录页
    }
  }
  NProgress.done()
})
// 后置守卫
router.afterEach(function() {
  NProgress.done() // 关闭进度条
})
