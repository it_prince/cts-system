
import Layout from '@/layout'

const examination = {
  path: '/examination',
  component: Layout,
  name: 'examination',
  meta: {
    title: '考试管理',
    icon: 'excel' },
  children: [
    {
      path: '',
      component: () => import('@/views/examination'),
      name: 'examination',
      meta: {
        title: '题库信息',
        icon: 'excel' }
    },
    {
      path: 'testpaper',
      component: () => import('@/views/examination/components/testpaper.vue'),
      name: 'testpaper',
      meta: {
        title: '试卷信息',
        icon: 'excel' }
    },
    {
      path: 'examInfo',
      component: () => import('@/views/examination/components/examInfo.vue'),
      name: 'examInfo',
      meta: {
        title: '考试信息',
        icon: 'excel' }
    }
  ]
}
export default examination
