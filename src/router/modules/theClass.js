
import Layout from '@/layout'

const theClass = {
  path: '/theClass',
  component: Layout,
  name: 'theClass',
  meta: {
    title: '班级管理',
    icon: 'excel' },
  children: [
    {
      path: '',
      component: () => import('@/views/theClass'),
      name: 'theClass',
      meta: {
        title: '班级信息',
        icon: 'excel' }
    },
    {
      path: 'classinfo',
      component: () => import('@/views/theClass/components/classinfo.vue'),
      name: 'classinfo',
      meta: {
        title: '学生信息',
        icon: 'excel' }
    }
  ]
}
export default theClass
