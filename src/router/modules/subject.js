
import Layout from '@/layout'

const subject = {
  path: '/subject',
  component: Layout,
  name: 'subject',
  meta: {
    title: '学科管理',
    icon: 'excel' },
  children: [
    {
      path: '',
      component: () => import('@/views/subject/'),
      name: 'subject',
      meta: {
        title: '学科信息',
        icon: 'excel' }
    },
    {
      path: 'directoryinfo',
      component: () => import('@/views/subject/components/directoryinfo.vue'),
      name: 'directoryinfo',
      meta: {
        title: '目录信息',
        icon: 'excel' }
    },
    {
      path: 'labelinfo',
      component: () => import('@/views/subject/components/labelinfo.vue'),
      name: 'labelinfo',
      meta: {
        title: '标签信息',
        icon: 'excel' }
    }
  ]
}
export default subject
