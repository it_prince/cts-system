
import Layout from '@/layout'

const permissions = {
  path: '/permissions',
  component: Layout,
  name: 'permissions',
  meta: {
    title: '权限管理',
    icon: 'excel' },
  children: [
    {
      path: '',
      component: () => import('@/views/permissions'),
      name: 'permissions',
      meta: {
        title: '菜单管理',
        icon: 'excel' }
    },
    {
      path: 'roleManagement',
      component: () => import('@/views/permissions/components/roleManagement.vue'),
      name: 'roleManagement',
      meta: {
        title: '角色管理',
        icon: 'excel' }
    },
    {
      path: 'user',
      component: () => import('@/views/permissions/components/user.vue'),
      name: 'user',
      meta: {
        title: '用户管理',
        icon: 'excel' }
    }
  ]
}
export default permissions
