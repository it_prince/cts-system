import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/layout'

// 导入动态路由
import Permissions from './modules/permissions'
import Subject from './modules/subject'
import TheClass from './modules/theClass'
import Examination from './modules/examination'

// 静态路由
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  // 将来404 这里要注释掉，要放在路由配置的最后
  { path: '*', redirect: '/404', hidden: true }
]

// 动态路由
export const asyncRoutes = [
  Permissions,
  Subject,
  TheClass,
  Examination
]

// 创建路由实例的函数
const createRouter = () => new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  // TODO: 没做权限之间
  routes: [...constantRoutes, ...asyncRoutes]
  // TODO: 做了权限之后
  // routes: [...constantRoutes]
})
// 创建路由实例
const router = createRouter()
// 重置路由实例
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
// 暴露路由实例
export default router
