// 导入vue
import Vue from 'vue'
// 导入i18插件
import VueI18n from 'vue-i18n'
// 给Vue安装i18插件
Vue.use(VueI18n)

// 导入自己的语言包
import zhLocale from './zh'
import enLocale from './en'
// 导入element-ui的语言包
import elEnLocale from 'element-ui/lib/locale/lang/en'
import elZhLocale from 'element-ui/lib/locale/lang/zh-CN'
// 导入js-cookie
import Cookie from 'js-cookie'

// 实例化一个i18n对象
const i18n = new VueI18n({
  // locale:'en', // 当前语言包的类型
  locale: Cookie.get('language') || 'zh',
  messages:{
    zh:{
      ...zhLocale,
      ...elZhLocale
    },
    en:{
      ...enLocale,
      ...elEnLocale
    }
  }
})
// 暴露
export default i18n